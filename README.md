DOOM Dark Theme for SideFX Houdini

Instructions:

1.
    - Download or git clone this repository to your desired download directory.

2.
    - Copy BOTH files: "DOOM_dark.hcs" & "NodeGraphDOOM.inc" into the directory "/home/houdini/config/" (edit this path to your own home directory and houdini version number)

3.
    - Open Houdini
    - In top menu select: "Edit" > "Color Settings" 
    - Select "DOOM Dark" in the "Color Scheme" dropdown menu. 
    - Click "Close"

4.
    - Go to "Edit" > "Preferences" > "General User Interface" 
    - Check "Color Pane Headers with Network Contexts"
    - Click "Accept"

Done!
